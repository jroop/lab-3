import java.util.Scanner;
public class Problem18 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int type;
        do{
            System.out.print("Please select Star Type [1-4,5 is Exit]: ");
            type = sc.nextInt();
            int size;
            if(type==1){
                System.out.print("Please input number: ");
                size = sc.nextInt();
                for(int i =0 ;i<size; i++){
                    for(int j = 0 ; j<=i ; j++){
                        System.out.print("*");
                    }
                    System.out.println();
                
                }

            }else if(type==2){
                System.out.print("Please input number: ");
                size = sc.nextInt();
                for(int i =0 ;i<size; i++){
                    for(int j = size ; j>i ; j--){
                        System.out.print("*");
                    }
                    System.out.println();
                }   
            }else if(type==3){
                System.out.print("Please input number: ");
                size = sc.nextInt();
                for(int i =0 ;i<size; i++){
                    for(int j=0; j<size ; j++){
                        if(j>=i){
                            System.out.print("*");
                        }else{
                            System.out.print(" ");
                        }
                    }
                    System.out.println();
                }
            }else if(type==4){
                System.out.print("Please input number: ");
                size = sc.nextInt();
                for(int i = size ;i>0; i--){
                    for(int j=0; j<size ; j++){
                        if((j+1)>=i){
                            System.out.print("*");
                        }else{
                            System.out.print(" ");
                        }
                    }
                    System.out.println();
                }
            }else{
                if(type==5){
                    System.out.print("Bye bye!!!");
                }else{
                    System.out.println("Error: Please input number between 1-5");
                }
            }      
        }while(type!=5);
    }
}
