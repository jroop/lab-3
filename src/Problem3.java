import java.util.Scanner;

public class Problem3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num1;
        int num2;
        System.out.print("Please input 1st Number : ");
        num1 = sc.nextInt();
        System.out.print("Please input 2nd Number : ");
        num2 = sc.nextInt();
        if(num1>num2){
            System.out.print("Error");
        } else{
            while(num1<=num2) {
                System.out.print(num1 + " ");
                num1++;
            }
        }
    }
}
