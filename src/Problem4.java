import java.util.Scanner;

public class Problem4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num;
        int sum=0;
        int count=0;
        do{
            System.out.print("Please input Number : ");
            num = sc.nextInt();
            if(num!=0){
                sum = sum + num ;
                count++;
                System.out.println("Sum :"+ sum +" Avg :"+((double)sum)/count);
            }    
        }while(num!=0);
        System.out.print("Bye");
        sc.close();
        
    }
}
